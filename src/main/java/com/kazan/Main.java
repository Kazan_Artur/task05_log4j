package com.kazan;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {

    private static Logger log = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        log.info("This is Info message");
        log.trace("This is Trace message");
        log.debug("This is Debug message");
        log.warn("This is Warn message");
        log.error("This is Error message");
        log.fatal("This is Fatal message");
    }
}